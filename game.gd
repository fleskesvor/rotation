extends Sprite

var counter = 0.0
var right = true

func _physics_process(delta):
	var direction = 1 if right else -1
	var rotation = direction * 5 * delta
	set_rotation(get_rotation() + rotation)

func _input(event):
	if event.is_pressed():
		if event.is_action("ui_left"):
			right = false
			set_physics_process(true)
		if event.is_action("ui_right"):
			right = true
			set_physics_process(true)
	else:
		set_physics_process(false)

func _ready():
	set_process_input(true)
