Rotation
========

A simple Godot demo project to experiment with different ways to make a sprite rotate on button input. The project has been kept in a single branch, but significant changes are tagged, to make working implementations easier to find.

* [discrete-frames](https://github.com/fleskesvor/rotation/releases/tag/discrete-frames) - Uses AnimatedSprite with one image per state and sets frame on each button press
* [continuous-frames](https://github.com/fleskesvor/rotation/releases/tag/discrete-frames) - Uses AnimatedSprite with one image per state and keeps changing frame as long as button is pressed

Use left arrow on keyboard to rotate left and right arrow to rotate left.

Current implementation uses a single image and a Sprite node, which is rotated as long as button is pressed. 
